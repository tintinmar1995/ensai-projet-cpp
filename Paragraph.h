/*
 * Paragraph.h
 *
 *  Created on: 29 mars 2016
 *      Author: tguyet
 */

#ifndef PARAGRAPH_H_
#define PARAGRAPH_H_


#include <vector>
#include <string>
#include "input.h"
#include "character.h"

class Paragraph {


public:
	Paragraph();
	virtual ~Paragraph();


	virtual int execute();
	int getnum() const;
	Character* pc;
	void setnum(int i);

	/* Methods for use by the builder (i.e. a Parser object) */
	void setDescription(const std::string& description) {
		_description = description;
	}

	string getDescription() {
			return _description;
		}

	void affiche();

	int next;
	/*int next() const;*/

	/**
	 * @brief Add a note to this paragraph that will be transferred to the
	 * player's character when they arrive
	 *
	 * @param note the note that the character will receive when they arrive
	 * on this paragraph
	 */
	void addNoteForCharacters(const std::string& note) {
		_notes.push_back(note);
	}

	void setC(Character* pointeurC) {
		this->pc = pointeurC;
	}

protected:
	/**
	 * @brief The text that is presented to the player when the character
	 * reaches this paragraph
	 */
	std::string _description;
	int _num;

	/**
	 * @brief The notes for the character when they arrive
	 */
	std::vector<std::string> _notes;
};

#endif /* PARAGRAPH_H_ */
