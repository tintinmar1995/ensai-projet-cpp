/**
 * @file input.cpp
 * @brief Implementation of the Input static class
 * @author Laurent Georget
 * @date 2016-03-19
 */

#include <iostream>
#include <limits>

#include "input.h"

std::istream& Input::pause(std::istream& in)
{
	int c = in.get();
	if (c != '\n')
		in >> reset;
	return in;
}

std::istream& Input::reset(std::istream& in)
{
	in.clear();
	in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	return in;
}
