/*
 * Character.h
 *
 *  Created on: 3 avr. 2018
 *      Author: id0883
 */

#ifndef CHARACTER_H_
#define CHARACTER_H_

#include <iostream>
#include <string>
#include <list>

using namespace std;

class Character {
public:
	Character();
	virtual ~Character();
	Character operator+=(const string &s);
	bool hasNote(const std::string& note) const;
	void push_back(string note);
	void push_back_hist(string note);
	int length_hist();
	list<string> historique;
	list<string> notes;

};

ostream &operator<<(ostream &os, const Character &cc);

#endif /* CHARACTER_H_ */

