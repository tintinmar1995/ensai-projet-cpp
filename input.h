/**
 * @file input.h
 * @brief Definition of the Input static class
 * @author Laurent Georget
 * @date 2016-03-19
 */

#ifndef INPUT_H
#define INPUT_H

#include <iostream>

/**
 * @brief A toolbox to manipulate input buffers for user interaction
 */
class Input
{
	public:
	/**
	 * @brief Pause the execution until the user press Enter
	 * @param in the input stream
	 * @return the input stream received as argument
	 */
	static std::istream& pause(std::istream& in);
	/**
	 * @brief Empty and clear the flags of an input buffer
	 * @param in the input stream
	 * @return the input stream received as argument
	 */
	static std::istream& reset(std::istream& in);
};

#endif

