/**
 * @file parser.h
 * @brief Definition of the Parser class
 * @author Laurent Georget
 * @date 2016-03-18
 */

#ifndef PARSER_H
#define PARSER_H

#include <iostream>
#include <string>

#include "paragraph.h"

/**
 * @brief Reads the files describing the paragraphs and builds the corresponding
 * Paragraph objects
 */
class Parser
{
public:
	/**
	 * @brief Builds a parser with the path where paragraphs are stored
	 *
	 * @param dir the directory where the paragraphs are stored
	 */
	Parser(std::string dir);

	/**
	 * @brief Builds one paragraph, identified by its number
	 *
	 * This methods allocates and builds a paragraph and returns a pointer
	 * on it. The caller is responsible for destroying the paragraph after
	 * use.
	 * @param index the index of the paragraph, which is also its filename
	 * @return the newly built paragraph
	 */
	Paragraph* buildOneParagraph(int index) const;

	static std::string extractQuotedSubstring(const std::string& s);

private:
	/**
	 * @brief The directory where the paragraphs are stored, once in each
	 * file
	 */
	std::string _dir;

	// A COMMENTER POUR LA PARTIE 3
	/*
	Paragraph* buildParagraph(std::istream& input) const;
	*/
	// FIN COMMENTER */

// A DECOMMENTER POUR LA PARTIE 3
	Paragraph* buildParagraphDirect(std::istream& input) const;
	Paragraph* buildParagraphRandom(std::istream& input) const;
	Paragraph* buildParagraphChoice(std::istream& input) const;
	Paragraph* buildParagraphState(std::istream& input) const;
};

#endif /* ifndef PARSER_H */
