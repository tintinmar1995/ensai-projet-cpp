/*
 * ParagraphState.h
 *
 *  Created on: 10 avr. 2018
 *      Author: id0883
 */

#ifndef PARAGRAPHSTATE_H_
#define PARAGRAPHSTATE_H_

#include <map>
#include <string>
#include "Paragraph.h"

namespace std {

class ParagraphState: public Paragraph {
public:
	ParagraphState();
	virtual ~ParagraphState();
	int execute();
	void addCondition(const std::string& condition,int paragraphe);

private:
	map<int,std::string> choix_possible;

};

} /* namespace std */

#endif /* PARAGRAPHSTATE_H_ */
