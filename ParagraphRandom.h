/*
 * ParagraphRandom.h
 *
 *  Created on: 10 avr. 2018
 *      Author: id0883
 */

#ifndef PARAGRAPHRANDOM_H_
#define PARAGRAPHRANDOM_H_

#include <string>
#include "world.h"
#include "Paragraph.h"

namespace std {

class ParagraphRandom: public Paragraph {
public:
	ParagraphRandom();
	virtual ~ParagraphRandom();
	void setRules(const std::string& competence,const std::string& level,const std::string& difficulty);
	void setOutcomes(int paragraphSuccess,int paragraphFailure);

	int execute();

private:
	string competence;
	string level;
	string difficulty;
	int success;
	int failure;
	int lancerDe(string nDm);
	int lancerDeManu(string nDm);
	int lancerDeAuto(string nDm);

};

} /* namespace std */

#endif /* PARAGRAPHRANDOM_H_ */
