/*
 * ParagraphState.cpp
 *
 *  Created on: 10 avr. 2018
 *      Author: id0883
 */

#include <map>
#include <string>
#include "ParagraphState.h"

namespace std {

ParagraphState::ParagraphState() {
	// TODO Auto-generated constructor stub

}

ParagraphState::~ParagraphState() {
	// TODO Auto-generated destructor stub
}

int ParagraphState::execute(){
	cout << *pc << endl;
	this->affiche();
	for (vector<string>::const_iterator it=_notes.begin(); it != _notes.end(); ++it){
		*pc +=(*it);
	}
	int paraSuiv = 0;
	int i = 0;
	string s;
	bool parDefaut = true;

	for (map<int,string>::const_iterator it=choix_possible.begin(); it != choix_possible.end(); ++it){
		i = it->first;
		s = it->second;

		for (vector<string>::const_iterator it2=_notes.begin(); it2 != _notes.end(); ++it2){
			if(s.compare(*it2) && parDefaut){
				paraSuiv = i;
				parDefaut = false;
			}
		}
		if(parDefaut && s.compare("")){
			paraSuiv = i;
		}
	}
	return paraSuiv;
}

void ParagraphState::addCondition(const std::string& condition,
	const int paragraphe) {
	choix_possible.insert ( std::pair<int,string>(paragraphe,condition) );
}

} /* namespace std */
