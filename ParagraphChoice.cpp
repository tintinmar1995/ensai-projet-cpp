/*
 * ParagraphChoice.cpp
 *
 *  Created on: 10 avr. 2018
 *      Author: id0883
 */

#include <map>
#include "ParagraphChoice.h"
#include "input.h"

namespace std {

ParagraphChoice::ParagraphChoice() {
	// TODO Auto-generated constructor stub

}

ParagraphChoice::~ParagraphChoice() {
	// TODO Auto-generated destructor stub
}

void ParagraphChoice::addChoice(int paragraphe, const std::string description) {
	choix_possible.insert ( std::pair<int,string>(paragraphe,description) );
}

int ParagraphChoice::execute(){
	cout << *pc << endl;
	this->affiche();
	for (vector<string>::const_iterator it=_notes.begin(); it != _notes.end(); ++it){
		*pc +=(*it);
	};

	for (map<int,string>::const_iterator it=choix_possible.begin(); it != choix_possible.end(); ++it){
		 std::cout << it->first << " => " << it->second << '\n';
		};

	int i;
		std::cout << "Entree une valeur en 0 et 73\n";
		do {
			std::cout << "saisie : ";
			if (!std::cin) {
				std::cin >> Input::reset;
			}
			std::cin >> i;
		} while (!std::cin || i > 73 || i < 0 || !inMap(i,choix_possible));
			std::cin >> Input::reset;
		return i;
	}

	bool ParagraphChoice::inMap(int i,map<int,std::string> liste) {
		bool retour = false;
		for (map<int,string>::const_iterator it=liste.begin(); it != liste.end(); ++it){
				 	 if(i== it->first){
				 		 retour = true;
				 	 }
				};
		return retour;
	}

} /* namespace std */
