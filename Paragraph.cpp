/*
 * Paragraph.cpp
 *
 *  Created on: 29 mars 2016
 *      Author: tguyet
 */

#include <iostream>
#include <list>
#include <string>
#include "parser.h"
#include "input.h"
#include "paragraph.h"
#include "character.h"

using namespace std;

Paragraph::Paragraph(){
	this->next=-1;
	this->_num=-1;
	this->pc = NULL;
}

Paragraph::~Paragraph() {}

int Paragraph::execute() {
	return -1;
}


void Paragraph::affiche() {
	cout << _description << endl;
}

int Paragraph::getnum() const {
	return _num;
}

void Paragraph::setnum(int i) {
	_num = i;
}

