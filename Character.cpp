/*
 * Character.cpp
 *
 *  Created on: 3 avr. 2018
 *      Author: id0883
 */

#include <iostream>
#include <string>
#include <list>
#include <vector>
#include "Character.h"

using namespace std;

Character::Character() {
	// TODO Auto-generated constructor stub

}

Character::~Character() {
	// TODO Auto-generated destructor stub
}

Character Character::operator +=(const string& s){
	this->push_back(s);
	return *this;
}

ostream &operator<<(ostream &os, const Character &cc) {
	for (list<string>::const_iterator it=cc.notes.begin(); it != cc.notes.end(); ++it){
			os <<" // " << (*it);
	}
    return os;
}

bool Character::hasNote(const std::string& note) const {
	bool retour=false;
	for (list<string>::const_iterator it=notes.begin(); it != notes.end(); ++it){
		      if(*it==note){
		    	  retour=true;
		      }
		 }
	return retour;
}


void Character::push_back(string note) {
	notes.push_back(note);
}

void Character::push_back_hist(string para) {
	historique.push_back(para);
}


int Character::length_hist() {
	return historique.size();
}
