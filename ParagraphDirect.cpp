/*
 * ParagraphDirect.cpp
 *
 *  Created on: 10 avr. 2018
 *      Author: id0883
 */

#include <string>
#include "ParagraphDirect.h"
#include "input.h"


namespace std {

ParagraphDirect::ParagraphDirect() {
	// TODO Auto-generated constructor stub

}

ParagraphDirect::~ParagraphDirect() {
	// TODO Auto-generated destructor stub
}

int ParagraphDirect::execute(){
	cout << *pc << endl;
	this->affiche();
	for (vector<string>::const_iterator it=_notes.begin(); it != _notes.end(); ++it){
		*pc +=(*it);
	};
	std::cout<< "Appuyez sur Enter pour continuer" <<std::endl;
	std::cin>>Input::pause;
	return this->next;
}

void ParagraphDirect::setNextParagraph(int next) {
	this->next = next;
}

} /* namespace std */
