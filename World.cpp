/*
 * World.cpp
 *
 *  Created on: 3 avr. 2018
 *      Author: id0883
 */


#include <string>
#include "parser.h"
#include "Paragraph.h"
#include "World.h"
#include "input.h"

using namespace std;

int World::modeJeu = 0;

World::World() {
	// TODO Auto-generated constructor stub
}

World::~World() {
	// TODO Auto-generated destructor stub

}


void World::run(int lastParagraph) {


	std::cout << "Quel mode de jeu choisissez-vous ?\n [1] D� r�el\n [2] D� virtuel"<<std::endl;

	int  m=0;
	do {
		std::cout << "Saisissez 1 ou 2"<<std::endl;
		if (!std::cin) {
			std::cin >> Input::reset;
		}
		std::cin >> m;
	} while (!std::cin || ((m !=1) && (m != 2)));
		std::cin >> Input::reset;
;

	World::modeJeu = m;
	Paragraph* p = parser.buildOneParagraph(0);
	p->setC(&c);
	int i;
	while (p->getnum() != lastParagraph){
		i = p->execute();
		p = parser.buildOneParagraph(i);
		c.push_back_hist(p->getDescription());
		p->setC(&c);
	}

	std::cout << "BRAVO ! Vous avez termin� l'histoire. Voici un retour sur votre aventure"<<std::endl;
	for (list<string>::const_iterator it=c.historique.begin(); it != c.historique.end(); ++it){
		std::cout << *it <<std::endl;
	}

}
