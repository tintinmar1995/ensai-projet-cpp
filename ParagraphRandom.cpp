/*
 * ParagraphRandom.cpp
 *
 *  Created on: 10 avr. 2018
 *      Author: id0883
 */

#include "ParagraphRandom.h"
#include "world.h"
#include <time.h>
#include <cstdlib>

namespace std {

ParagraphRandom::ParagraphRandom() {
	// TODO Auto-generated constructor stub
	this->competence="";
	this->level="";
	this->difficulty="";
	this->success=-1;
	this->failure=-1;

}

ParagraphRandom::~ParagraphRandom() {
	// TODO Auto-generated destructor stub
}

void ParagraphRandom::setRules(const std::string& competence,
		const std::string& level, const std::string& difficulty) {
	this->competence = competence;
	this->level = level;
	this->difficulty = difficulty;
}

void ParagraphRandom::setOutcomes(int paragraphSuccess, int paragraphFailure) {
	this->success = paragraphSuccess;
	this->failure = paragraphFailure;
}

int ParagraphRandom::execute() {
	cout << *pc << endl;
	this->affiche();
	for (vector<string>::const_iterator it=_notes.begin(); it != _notes.end(); ++it){
		*pc +=(*it);
	}
	int lanceDe = lancerDe(level);
	int difficulte=0;

	//on compare � la difficult�
	if(difficulty.length() > 1 && this->difficulty[1] == 'D'){
		std::cout << "Le choix de la difficult� sera le r�sultat d'un nouveau lanc� de d�.";
		difficulte = lancerDe(difficulty);
	}else{
		difficulte =  std::atoi(difficulty.c_str());
	}
	std::cout << "La difficult� vaut "<<difficulty << std::endl;

	if(World::modeJeu == 2){
		std::cout<< "Appuyez sur Enter pour continuer" <<std::endl;
		std::cin>>Input::pause;
	}

	if (lanceDe >= difficulte){
		return this->success;
	}else{
		return this->failure;
	}

}

int ParagraphRandom::lancerDeManu(string nDm){
	int nbdice = nDm[0] - '0';
	int min_value = nbdice;
	int max_value = 6*nbdice;
	if(nDm.length()>2){
		min_value += nDm[3] - '0';
		max_value += nDm[3] - '0';
	}
	//on demande � l'utilisateur de rentrer le r�sultat de son lanc� de d�
	int lanceDe=0;
	do {
		std::cout << "Entrez le r�sultat de votre lanc� de d�(s)";
		if (!std::cin) {
			std::cin >> Input::reset;
		}
		std::cin >> lanceDe;
	} while (!std::cin || lanceDe > max_value || lanceDe < min_value);
		std::cin >> Input::reset;
	return lanceDe;
}

int ParagraphRandom::lancerDeAuto(string nDm){
	srand (time(NULL));
	int nbdice = nDm[0] - '0';
	int min_value = nbdice;
	int max_value = 6*nbdice;
	if(nDm.length()>2){
		min_value += nDm[3] - '0';
		max_value += nDm[3] - '0';
	}
	int lanceDe = 0;
	for (int i=0;i<nbdice;i++){
		lanceDe += (rand() % 6 + 1);
	}
	if(nDm.length()>2){
		lanceDe += nDm[3] - '0' ;
	}
	std::cout<<"Lancer de d�(s) : "<<lanceDe<<std::endl;
	return lanceDe;
}

int ParagraphRandom::lancerDe(string nDm){
	if(World::modeJeu == 1){
			return lancerDeManu(nDm);
		}else{
			return lancerDeAuto(nDm);
		}
}

} /* namespace std */
