/*
 * ParagraphDirect.h
 *
 *  Created on: 10 avr. 2018
 *      Author: id0883
 */

#ifndef PARAGRAPHDIRECT_H_
#define PARAGRAPHDIRECT_H_

#include "Paragraph.h"
#include "input.h"

namespace std {

class ParagraphDirect: public Paragraph {
public:
	ParagraphDirect();
	virtual ~ParagraphDirect();
	void setNextParagraph(int next);
	int execute();
};

} /* namespace std */

#endif /* PARAGRAPHDIRECT_H_ */
