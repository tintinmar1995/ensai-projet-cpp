/**
 * @file main.cpp
 * @brief Entry point of the program
 * @author Laurent Georget
 * @date 2016-03-18
 */

#include <cstdlib>
#include <iostream>
#include "input.h"
#include "parser.h"
#include "World.h"



int main(int argc, char** argv)
{

	World monde;
	monde.run(47);

}
