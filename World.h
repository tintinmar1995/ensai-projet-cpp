/*
 * World.h
 *
 *  Created on: 3 avr. 2018
 *      Author: id0883
 */

#ifndef WORLD_H_
#define WORLD_H_

#include <string>
#include "parser.h"
#include "Paragraph.h"
#include "World.h"
#include "input.h"

using namespace std;

class World {
public:
	World();
	virtual ~World();
	void run(int lastParagraph);
	static int modeJeu;
	Parser parser = Parser("reginacayli");
	Character c;
};


#endif /* WORLD_H_ */
