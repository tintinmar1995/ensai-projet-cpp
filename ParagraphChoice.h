/*
 * ParagraphChoice.h
 *
 *  Created on: 10 avr. 2018
 *      Author: id0883
 */

#ifndef PARAGRAPHCHOICE_H_
#define PARAGRAPHCHOICE_H_

#include <string>
#include <map>
#include "Paragraph.h"


namespace std {

class ParagraphChoice: public Paragraph {
public:
	ParagraphChoice();
	virtual ~ParagraphChoice();
	void addChoice(int paragraphe,const std::string description);
	int execute();
	bool inMap(int i,map<int,std::string> liste);

private:
	map<int,std::string> choix_possible;
};

} /* namespace std */

#endif /* PARAGRAPHCHOICE_H_ */
