/**
 * @file parser.cpp
 * @brief Implementation of the Parser class
 * @author Laurent Georget
 * @date 2016-03-18
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <limits>
#include <exception>
#include <stdexcept>

#include "parser.h"

#include "paragraphdirect.h"
#include "paragraphrandom.h"
#include "paragraphstate.h"
#include "paragraphchoice.h"


Parser::Parser(std::string dir) :
	_dir(dir)
{}

std::string Parser::extractQuotedSubstring(const std::string& s)
{
	int pos = s.find_first_of('"') + 1;
	int count = s.find_first_of('"', pos) - pos;
	return (count > 0) ? s.substr(pos, count) : std::string();
}

Paragraph* Parser::buildOneParagraph(int index) const
{
	//Opening the file
	char buf[50];
	sprintf(buf, "%d", index);
	std::string filename = _dir + "/" + buf;
	std::ifstream file(filename.c_str());
	std::cerr << "+++ Building paragraph " << index << std::endl;
	if (!file)
		throw std::domain_error("'" + filename + "' is not a valid paragraph file");

	//read description lines until an empty line is detected
	std::string description;
	std::string buffer;
	do {
		std::getline(file, buffer);
		if (!buffer.empty())
			description += buffer + "\n";
	} while(!buffer.empty()); //we read until the first empty line


	// Next, we extract the commands
	// there can be any number of 'Note' command followed by one of
	// { 'Direct', 'Choix', 'Jet', 'Etat' }
	//
	std::string command;
	std::vector<std::string> notes;

	file >> command;
	while (!file.eof() && command == "Note")
	{
		getline(file, buffer);
		notes.push_back(extractQuotedSubstring(buffer));
		std::cerr << "+++ One note found " << extractQuotedSubstring(buffer) << std::endl;
		file >> command;
	}

	//Create the actual paragraph
	 Paragraph* res;
	if (command == "Direct") {
		res = buildParagraphDirect(file);
	} else if (command == "Choix") {
		res = buildParagraphChoice(file);
	} else if (command == "Jet") {
		res = buildParagraphRandom(file);
	} else if (command == "Etat") {
		res = buildParagraphState(file);
	} else {
		throw std::invalid_argument("'" +  command + "' is not a valid command");
	}


	// A COMMENTER POUR LA PARTIE 3
	/*
	Paragraph* res = buildParagraph(file);
	*/
	// FIN COMMENTER */

	//Set values to the paragraph
	res->setDescription( description );
	for (std::vector<std::string>::iterator it = notes.begin() ;
	     it != notes.end() ;
	     ++it) {
		res->addNoteForCharacters(*it);
	}
	res->setnum(index);
	return res;
}

// A COMMENTER POUR LA PARTIE 3
/*
Paragraph* Parser::buildParagraph(std::istream& input) const
{
	Paragraph* p = new Paragraph();
	std::cerr << "+++ The paragraph is a Standard" << std::endl;
	return p;
}
*/
// FIN COMMENTER */


// A DECOMMENTER POUR LA PARTIE 3
Paragraph* Parser::buildParagraphDirect(std::istream& input) const
{
	ParagraphDirect* p = new ParagraphDirect();
	int next;
	input >> next;
	p->setNextParagraph(next);
	std::cerr << "+++ The paragraph is a Direct, next: " << next << std::endl;
	return p;
}

Paragraph* Parser::buildParagraphRandom(std::istream& input) const
{
	ParagraphRandom* p = new ParagraphRandom();
	std::string competence;
	std::string level;
	std::string difficulty;
	int nextIfSuccess;
	int nextIfFailure;
	input >> competence >> level >> difficulty;
	p->setRules(competence, level, difficulty);
	input >> nextIfSuccess >> nextIfFailure;
	p->setOutcomes(nextIfSuccess, nextIfFailure);
	std::cerr << "+++ The paragraph is a Random: " << competence << " | "
		  << level << " | " << difficulty << " | "
		  << nextIfSuccess << " | " << nextIfFailure
		  << std::endl;
	return p;
}

Paragraph* Parser::buildParagraphChoice(std::istream& input) const
{
	ParagraphChoice* p = new ParagraphChoice();
	while (input && !input.eof()) {
		int next;
		input >> next;
		input.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::string choice;
		std::string buffer;
		do {
			getline(input,buffer);
			choice += buffer + "\n";
		} while (input && !input.eof() && !buffer.empty());
		p->addChoice(next, choice);
	}

	std::cerr << "+++ The paragraph is a Choice" << std::endl;
	return p;
}

Paragraph* Parser::buildParagraphState(std::istream& input) const
{
	ParagraphState* p = new ParagraphState();
	while (input && !input.eof()) {
		int next;
		input >> next;
		input.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::string note;
		getline(input, note);
		p->addCondition(extractQuotedSubstring(note), next);
	}

	std::cerr << "+++ The paragraph is a State" << std::endl;
	return p;
}



